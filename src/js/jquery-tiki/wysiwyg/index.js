import "summernote";
import initSummernote from "./initSummernote";

export default initSummernote;

export { default as loadLanguage } from "./loadLanguage";
export { default as inlineEdit } from "./inlineEdit";
